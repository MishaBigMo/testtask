//
//  ViewController.swift
//  999
//
//  Created by Михаил on 26/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - Properties
    let productCollectionViewCellid = "ProductCollectionViewCell"
    let categoriesCollectionViewCellId = "CategoriesCollectionViewCell"
    var products: [ProsuctDto] = []
    var categories: [Categories] = []
    
    // MARK: - Outlets
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    // MARK: - Actions
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibCell = UINib(nibName: productCollectionViewCellid, bundle: nil)
        productCollectionView.register(nibCell, forCellWithReuseIdentifier: productCollectionViewCellid)
        
        let nibCategoriesCell = UINib(nibName: categoriesCollectionViewCellId, bundle: nil)
        categoriesCollectionView.register(nibCategoriesCell, forCellWithReuseIdentifier: categoriesCollectionViewCellId)
        
        let categories1 = Categories()
        categories1.name = "Pizza"
        categories1.count = "2350 places"
        categories1.image = UIImage(named: "pizza")
        categories.append(categories1)
        
        let categories2 = Categories()
        categories2.name = "Burgers"
        categories2.count = "350 places"
        categories2.image = UIImage(named: "hamburger")
        
        categories.append(categories2)
        
        let categories3 = Categories()
        categories3.name = "Steak"
        categories3.count = "834 places"
        categories3.image = UIImage(named: "meat")
        
        categories.append(categories3)
        
        let categories4 = Categories()
        categories4.name = "Pasta"
        categories4.count = "150 places"
        categories4.image = UIImage(named: "spaguetti")
        
        categories.append(categories4)
        
        let product1 = ProsuctDto()
        product1.image = UIImage(named: "image1")
        product1.imageRank = UIImage(named: "rank")
        product1.title = "Rahat Brasserie"
        product1.subtitle = "124 Levent/Besiktas"
        product1.rankNumber = "4.9"
        product1.rank = "(120 retings)"
        products.append(product1)
        
        let product2 = ProsuctDto()
        product2.image = UIImage(named: "image2")
        product2.imageRank = UIImage(named: "rank")
        product2.title = "Garage Bar"
        product2.subtitle = "124 Levent/Besiktas"
        product2.rankNumber = "4.9"
        product2.rank = "(120 retings)"
        products.append(product2)
        
        
        categoriesCollectionView.reloadData()
        productCollectionView.reloadData()
    }
}



extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.productCollectionView {
            return products.count
        }
        
        return categories.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.productCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCollectionViewCellid, for: indexPath) as! ProductCollectionViewCell
            let product = products[indexPath.row]
            cell.title.text = product.title
            cell.subtitle.text = product.subtitle
            cell.rankNumber.text = product.rankNumber
            cell.rank.text = product.rank
            cell.imageRank.image = product.imageRank
            cell.image.image = product.image
            return cell
        }
        else {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: categoriesCollectionViewCellId, for: indexPath) as? CategoriesCollectionViewCell
            let categorie = categories[indexPath.row]
            cellB?.name.text = categorie.name
            cellB?.count.text = categorie.count
            cellB?.image.image = categorie.image
            return cellB!
        }
    }
    
    
}
