//
//  RoundButton.swift
//  999
//
//  Created by Михаил on 26/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import UIKit
import Foundation

class RoundButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
    
}
