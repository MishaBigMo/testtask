//
//  CategoriesCollectionViewCell.swift
//  999
//
//  Created by Михаил on 26/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var view: UIView! {
        didSet {
            view.layer.cornerRadius = 10
            view.layer.shadowOffset = CGSize(width: 0, height: 0)
            view.layer.shadowOpacity = 0.2
            view.layer.shadowRadius = 4
        }
    }
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var count: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
