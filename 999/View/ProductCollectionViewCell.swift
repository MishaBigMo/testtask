//
//  ProductCollectionViewCell.swift
//  999
//
//  Created by Михаил on 26/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView! {
        didSet {
            image.layer.cornerRadius = 8
            image.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var rankNumber: UILabel!
    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var imageRank: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
