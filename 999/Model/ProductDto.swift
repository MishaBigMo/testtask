//
//  ProductDto.swift
//  999
//
//  Created by Михаил on 26/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import UIKit
import Foundation

class ProsuctDto {
    
    var image: UIImage?
    var title: String?
    var subtitle: String?
    var rankNumber: String?
    var rank: String?
    var imageRank: UIImage?
    
    required init() {}
    
}
